from django.apps import AppConfig


class Lab9Config(AppConfig): # pragma: no cover
    name = 'lab_9'
