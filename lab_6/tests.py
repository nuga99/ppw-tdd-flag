from django.test import TestCase, Client
from django.urls import resolve
from django.utils import timezone
from django.http import HttpRequest

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

from lab_6 import views
from .models import Message
from .forms import MessageForm
import unittest

# Create your tests here.
class Lab6TestCase(TestCase):

    # check urls endpoint localhost:8000/lab-6/
    def test_lab6_url_is_exist(self):
        response = Client().get('/lab-6/')
        self.assertEqual(response.status_code, 200)

    # check views.py using "add_message" function
    def test_lab6_using_function(self):
        get_func = resolve('/lab-6/')
        self.assertEqual(get_func.func, views.add_message)

    # check create status message
    def test_lab6_create_model_status_message(self):
        create_status_messages = Message.objects.create(
            message="Aku hari ini sedang makan buah", created_at=timezone.now())
        counting_all_messages = Message.objects.all().count()
        self.assertEqual(counting_all_messages, 2)

    # setUp forms lab_6 for forms.py
    def setUp(self):
        self.message = Message.objects.create(
            message="Aku hari ini sedang makan buah")

    # test_init MessageForm for entry
    def test_init(self):
        MessageForm(self.message)

        # check message forms valid
    def test_lab6_messages_forms_valid(self):
        message_valid = MessageForm(
            data={'message': 'Hello, apa kabar', 'created_at': timezone.now()})
        self.assertTrue(message_valid.is_valid())

    # check messages forms invalid
    def test_lab6_messages_forms_invalid(self):
        message_valid = MessageForm(
            data={'message': "", 'created_at': timezone.now()})
        self.assertFalse(message_valid.is_valid())

    # check post request save to database
    def test_lab6_can_save_POST_request(self):
        response = Client().post('',
                                 data={'message': 'Whatsitooya?', 'created_at': timezone.now()})
        count_all_message_status = Message.objects.all().count()
        self.assertEqual(count_all_message_status, 1)
        self.assertEqual(response.status_code, 302)  # redirect status code
        self.assertEqual(response['location'], '/lab-6/')

    # check session and html tags response
    def test_lab6_used_html_tags_response(self):
        response = Client().get('/lab-6/')
        self.assertContains(response, '<form', 1)
        self.assertContains(
            response, '<div class="table-responsive">', status_code=200)
        self.assertContains(
            response, '<div class="pagination', status_code=200)

    # check "Hello apa kabar?" in add_message function from views.py
    def test_lab6_message(self):
        request = HttpRequest()
        response = views.add_message(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Hello apa kabar ?', html_response)


class Lab6ProfileTestCase(TestCase):

    # check urls used by profile page
    def test_lab6_profile_urls(self):
        response = Client().get('/lab-6/profile/')
        self.assertEqual(response.status_code, 200)

    # check function used profile page
    def test_lab6_profile_function_used(self):
        response = resolve('/lab-6/profile/')
        self.assertEqual(response.func, views.profile)

    # check template used for profile html
    def test_lab6_profile_template_used(self):
        response = Client().get('/lab-6/profile/')
        self.assertTemplateUsed(response, 'profile.html')

    # check tags html response that used in profile page

    def test_lab6_profile_html_tags_response(self):
        response = Client().get('/lab-6/profile/')
        self.assertContains(response, '<img id="img-responsive"', 6)
        self.assertContains(response, '<p id="short-intro">', 1)
        self.assertContains(response, '<h2>About</h2>', 1)


class Lab8TestCase(TestCase):
    def test_check_navbar(self):
        response = Client().get('/lab-6/profile/')
        self.assertContains(response,'<nav class="navbar',1)

    def test_check_image(self):
        response = Client().get('/lab-6/profile/')
        self.assertContains(response, '<img id="img-responsive"', 6)

    def test_check_javascript(self):
        response = Client().get('/lab-6/profile/')
        self.assertContains(response, '<script src="/static/js/include.js"', 1)

    def test_accordion_used(self):
        response = Client().get('/lab-6/profile/')
        self.assertContains(response,'<div id="accordion"',1)

