from django.urls import path
from lab_6 import views

urlpatterns = [
    path('', views.add_message , name="add_status"),
    path('profile/',views.profile , name="profile_page")
]