$(document).ready(function() {
  $("#accordion").accordion({
    header: "h2",
    collapsible: true,
    heightStyle: "content",
    navigation: true
  });

  $(".ui-accordion-content").css("background-color", "#f7b733");
  $(".ui-accordion-header").css("background-color", "#f7b733");

  var counter = 1;
  document.getElementById("switch").onclick = function() {
    counter += 1;

    if (counter % 2 != 0) {
      document.getElementById("theme").href = "/static/css/style.css";
      $(".ui-accordion-header").css("background-color", "#f7b733");
      $("h2").css("color", "#000000");
    } else {
      document.getElementById("theme").href = "/static/css/dark.css";
      $(".ui-accordion-header").css("background-color", "#4169e1");
      $("h2").css("color", "#FFFFFF");
    }
  };

});
