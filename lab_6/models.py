from django.db import models
from django.utils import timezone
# Create your models here.

class Message(models.Model):
    message = models.CharField(max_length=300)
    created_at = models.DateTimeField(auto_now=True)
