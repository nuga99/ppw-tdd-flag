from django.test import TestCase, Client

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

import unittest
import time

# pragma: no cover
class FunctionalTestLab7(TestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome(
            './chromedriver', chrome_options=chrome_options)
        super(FunctionalTestLab7, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(FunctionalTestLab7, self).tearDown()

    def test_coba_coba_status(self):
        status_form_input = "Coba Coba"
        selenium = self.selenium
        selenium.get('http://ppw-c-ppw-tdd.herokuapp.com/lab-6/')

        time.sleep(5)

        status_form = selenium.find_element_by_id('id_message')
        submit_button = selenium.find_element_by_id('submit-button')

        status_form.send_keys(status_form_input)
        submit_button.send_keys(Keys.RETURN)

        time.sleep(5)

        status_form_info = selenium.find_elements_by_id(
            'status-message-col')[0]  # the newest status
        self.assertEqual(status_form_input, status_form_info.text)

    # test for body positioning
    def test_css_body_position(self):
        selenium = self.selenium
        body_tag = selenium.find_element_by_tag_name("body")
        location = body_tag.location
        size = body_tag.size

        self.assertEqual(size.get('height'),584)
        self.assertEqual(size.get('width'),784)
        self.assertEqual(location.get('x'),8)
        self.assertEqual(location.get('y'),8)

    # test for image profile positioning
    def test_image_profile_position(self):
        selenium = self.selenium
        selenium.get('http://ppw-c-ppw-tdd.herokuapp.com/lab-6/profile/')

        time.sleep(5)

        profile_pos = selenium.find_element_by_tag_name("img")
        location = profile_pos.location
        size = profile_pos.size

        self.assertEqual(size.get('height'),264)
        self.assertEqual(size.get('width'),264)
        self.assertEqual(location.get('x'),260)
        self.assertEqual(location.get('y'),200)


    def test_profile_status_css(self):
        selenium = self.selenium
        selenium.get('http://ppw-c-ppw-tdd.herokuapp.com/lab-6/')

        time.sleep(5)

        status_msg = selenium.find_element_by_class_name("status")
        position_status_message = status_msg.value_of_css_property('text-align')
        position_type_status_message = status_msg.value_of_css_property('position')
        self.assertEqual(position_type_status_message, "absolute")
        self.assertEqual(position_status_message, "center")


    def test_h2_description_profile(self):
        selenium = self.selenium
        selenium.get('http://ppw-c-ppw-tdd.herokuapp.com/lab-6/profile/')

        time.sleep(5)

        profile_description = selenium.find_element_by_tag_name('h2')
        position_profile_desc = profile_description.value_of_css_property('text-align')

        self.assertEqual(position_profile_desc,"center")


class FunctionalTestLab8():# pragma: no cover
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome(
            './chromedriver', chrome_options=chrome_options)
        super(FunctionalTestLab7, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(FunctionalTestLab7, self).tearDown()

if __name__ == '__main__':  # pragma: no cover
    unittest.main(warnings='ignore')  #
