from django.contrib import admin
from .models import Message
# Register your models here.

class MessageAdmin(admin.ModelAdmin):
    list_display = ('message', 'created_at')
    list_display_links = ('message',)
    search_fields = ('message',)


admin.site.register(Message,MessageAdmin)