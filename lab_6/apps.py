from django.apps import AppConfig


class Lab6Config(AppConfig): # pragma: no cover
    name = 'lab_6'
