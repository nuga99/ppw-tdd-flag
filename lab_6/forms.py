from django import forms
from django.db import models
from django.forms import ModelForm,fields
from .models import Message

class MessageForm(forms.ModelForm):

    class Meta:
        model = Message
        fields = ['message']
        widgets = {
            'message': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Isi pesan status', 'maxlength':300}),
            'created_at': forms.HiddenInput() #hidden input
        }