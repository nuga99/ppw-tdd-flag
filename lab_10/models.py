from django.db import models

# models of regist subscriber
class RegisterSubscribe(models.Model):
    email = models.EmailField(unique=True)
    nama = models.CharField(max_length=30)
    password = models.CharField(max_length=30)