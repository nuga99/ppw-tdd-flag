from django import forms
from .models import RegisterSubscribe

# implement forms using ModelForm
class RegisterSubscribeForm(forms.ModelForm):
    class Meta:
        model = RegisterSubscribe
        fields = ['email', 'nama', 'password']
        widgets = {

            'email': forms.TextInput(
                attrs={'type':'email','class': 'form-control', 'id':'email_form','placeholder': 'Isi email', 'maxlength': 50}),

            'nama': forms.TextInput(
                attrs={'class': 'form-control', 'id':'nama_form', 'placeholder': 'Isi nama' , 'maxlength': 30}),

            'password': forms.TextInput(
                attrs={ 'type':'password' ,'class': 'form-control', 'id':'password_form','placeholder': 'Isi password', 'maxlength': 30}),
        }
